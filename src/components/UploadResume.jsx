import React, { useContext } from "react";
import AppContext from "./AppContext";
import "./FileUpload2.css";

const UploadResume = () => {
  const { resume, setResume } = useContext(AppContext);



  const handleChange2 = (e) => {
    const reader1 = new FileReader();

    reader1.onload = () => {
      if (reader1.readyState === 2) {
        setResume(reader1.result);

      }
    };
    const fetchFile = 
     () => {
     fetch( "http://localhost:8081/company/uploadStudentImageFile/0",  {
        method: "post",
        headers: {
          "content-type": "multipart/form-data; boundary=something"
        },
        body:    reader1.readAsDataURL(e.target.files[0])
      })
        .then((res) => res.json())
        .then((data) => {
          console.log(data);
        })
        .catch((err) => err.message);
        console.log(reader1.result);
    }
    fetchFile()
    

  };
  

  return (
    <div className="page">
      <div className="container">
        <div className="img-holder">
          <img src={resume} alt="" id="img" className="img" />
        </div>
        <input
          type="file"
          accept="image/jpeg,image/gif,image/png,application/pdf,image"
          name="image-upload2"
          id="input2"
          onChange={handleChange2}
        />
        <div color="primary">
          <label
            className="image-upload2"
            htmlFor="input2"
            style={{ background: "primary" }}
          >
            Ladda upp CV
          </label>
        </div>
      </div>
    </div>
  );
};

export default UploadResume;
