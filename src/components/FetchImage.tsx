import React, {useEffect, useState} from "react";
import {createStyles, makeStyles} from "@material-ui/core/styles";

interface FileInterface {
    "name": string
    "url": string
    "type": "image/jpeg",
    "size": number
}

const useStyles = makeStyles((theme) =>
    createStyles({
        rootFetchImage: {
            width: "200px",
        },
    })
);

const FetchFile =()=> {
    const classes = useStyles();
    const [dataBundle, setDataBundle] = useState<FileInterface[]>();
    useEffect(() => {
        fetchData();
    }, [])

    const fetchData = async () => {
        await fetch('http://localhost:8081/files')
            .then(item => item.json())
            .then(blob=>{
                setDataBundle(blob)
                console.log(blob)
            });
    }

    return(
        <div>
            {dataBundle?.map(item=>{
                return(
                    <div>
                        <img src={item.url} className={classes.rootFetchImage} alt=""/>
                    </div>
                )})}

        </div>
    )
}

export default FetchFile;