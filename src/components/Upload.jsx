import axios from "axios";
import { useContext, useEffect, useState } from "react";
import AppContext from "./AppContext";
import "./FileUpload.css";

const Upload = () => {
  const [flag, setflag] = useState(false);
  const [imageId, setImageId] = useState(null);
  const { profile, setProfile, setStudent, student } = useContext(AppContext);

  let formData = new FormData();



  const onFileChange = (e) => {
    if (e.target && e.target.files[0]) {
      formData.append("file", e.target.files[0]);
    }
  };
  const submitFileData = (e) => {
    const headers = {
      "Content-Type": "multipart/form-data",
    };
    axios
      .post(
        "http://localhost:8081/uploadStudentImageFile/0",

        formData,
        {
          headers: headers,
        }
      )
      .then((res) => {
        console.log("res = ", res);

        setflag(true);
      })
      .catch((err) => console.log(err));
  };

  useEffect(() => {
    fetch("http://localhost:8081/students/getStudentById/0")
      .then((res) => res.json())
      .then((student) => {
        setStudent(student);
        setImageId(student.imageId);
        console.log(student);
      })

      .catch((err) => err.message);
  }, [flag, setImageId, setProfile]);


  const getImage = () => {
    const headers = {
      "Content-Type": "multipart/form-data",
    };

    fetch(
      "http://localhost:8081/files/" + imageId,

      formData,
      {
        headers: headers,
      }
    )
      .then((res) => {
        console.log("res = ", res);
        setProfile(res.url);
      })
      .catch((err) => console.log(err));
  };

  return (
    <div className="page">
      <div className="container">
        <div className="img-holder">
          <img
            src={profile}
            alt=" spara uppdatering nedan"
            id="img"
            className="img"
          />
        </div>

        <div color="primary">
          <input
            type="file"
            className="image-upload"
            onChange={onFileChange}
            style={{ background: "primary", marginBottom: "20px" }}
          />


          {
            !flag ?
                      <label
                      className="image-upload"
                      onClick={submitFileData}
                      style={{ background: "primary", marginBottom: "20px" }}
                    >
                      Spara!
                    </label>

                    : null
          }


          {flag && student.imageId != null ? (
            <label
              className="image-upload"
              onClick={getImage}
              style={{ background: "primary" }}
            >
              updatera till ny profil
            </label>
          ) : null}
        </div>
      </div>
    </div>
  );
};

export default Upload;
