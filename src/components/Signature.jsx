import { Button } from '@material-ui/core'
import React, { Component } from 'react'
import SignaturePad from 'react-signature-canvas'



 class Signature extends Component {

  state = {trimmedDataURL: null}
  
  sigPad = {}
  clear = () => {
    this.sigPad.clear()
  }
  trim = () => {
    this.setState({trimmedDataURL: this.sigPad.getTrimmedCanvas()
      .toDataURL('image/png')})
  }
  render () {
    let {trimmedDataURL} = this.state
    return <div >
      <div >
        <SignaturePad  backgroundColor="rgba(	0, 0, 255, 0.1)"   color="primary"  
          ref={(ref) => { this.sigPad = ref }} />
      </div>
      <div>
        <Button  variant="contained" color="primary"  style={{marginRight:"30px"}} onClick={this.clear}>
          Gör om
        </Button>
        <Button  variant="contained" color="primary"    onClick={this.trim}>
          Spara
        </Button>
      </div>

      {trimmedDataURL
        ? <img 
          src={trimmedDataURL} />
        : null}
    </div>
  }
}

export default Signature;