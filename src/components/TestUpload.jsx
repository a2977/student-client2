
import React from 'react'
import axios from 'axios';

const TestUpload = () => {


    let formData = new FormData();

    

     const onFileChange = (e) => {
        console.log(e.target.files[0])
        if(e.target && e.target.files[0]){
            formData.append("file", e.target.files[0]);
        }
    }

     const submitFileData = (e) => {
        const headers = {
            'Content-Type': 'multipart/form-data'
          }
         axios.post(
             "http://localhost:8081/company/uploadStudentImageFile/0",
             formData, {
                 headers: headers
             }
         ).then(res => console.log(res))
         .catch(err => console.log(err))
    }
    
    
    return (
        <div>
            <input type="file" name="file_upload" onChange={onFileChange} />
            <div>
                <button onClick={submitFileData} > submit data </button>
            </div>
        </div>
    )
}

export default TestUpload
