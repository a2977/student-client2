import React from "react";

import { AppBar,  Toolbar } from "@material-ui/core";
import KeyboardBackspaceIcon from "@material-ui/icons/KeyboardBackspace";

import Typography from "@material-ui/core/Typography";
import { useHistory } from "react-router";

const NavBar = () => {
  const history = useHistory();
  return (
    <div>
      <AppBar position="relative" style={{ marginBottom: "120px" }}>
        <Toolbar>
          <KeyboardBackspaceIcon onClick={() => history.push("/table")} />
          <Typography
            variant="h5"
            color="inherit"
            align="center"
            style={{ width: "100%" }}
          >
            Ansök
          </Typography>
        </Toolbar>
      </AppBar>
    </div>
  );
};



export default NavBar;
