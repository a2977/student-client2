import React, { useContext, useState } from "react";
import { createStyles, Theme, makeStyles } from "@material-ui/core/styles";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import ListSubheader from "@material-ui/core/ListSubheader";
import Checkbox from "@material-ui/core/Checkbox";
import { Button, Container } from "@material-ui/core";
import AppContext from "./AppContext";
import { useHistory } from "react-router";

import CircularProgress from '@material-ui/core/CircularProgress';


const ApplyList = () => {


const [loading, setLoading] = useState(false)
  
  const classes = useStyles();
  const {
    job1,
    job2,
    job3,
    job4,
    job5,
    sparaJob1,
    sparaSetJob1,
    favoriteList,
    setFavoriteList,
    allCompanies,
    student
  } = useContext(AppContext);
  const applyJobList = [job1, job2, job3].filter(
    (job) => job.favorite === true && student.favorites.includes(job1.company)    
  );
  const handleSubmit = (e) => {
    e.preventDefault();
    console.log(applyJobList)
    setLoading(true)
    setSubmited(true)
    setTimeout(() => {
       history.push("klart")
       setLoading(false)
    }, 3000)
    
   
 }

  const [checked, setChecked] = useState(false);
  const [submited, setSubmited] = useState(false)
  const history = useHistory();


  return (
    <>
      {applyJobList <= 0 ? (
        <p style={{ color: "#ff0000" }}>
          Stjärna ett företag för att komma vidare
        </p>
      ) : (
        <p>Du har sökt till följande</p>
      )}
      <List
        sx={{
          width: "100%",
          maxWidth: 360,
          bgcolor: "background.paper",
          position: "relative",
          overflow: "auto",
          maxHeight: 300,
          "& ul": { padding: 0 },
        }}
        subheader={<li />}
      >
        {applyJobList.map((job) => (
          <li key={`${job.id}`}>
            <ul>
              {checked && applyJobList ? (
                <ListSubheader>
                  {` ${job.company} - ${job.profession} `}
                </ListSubheader>
              ) : null}
            </ul>
          </li>
        ))}
      </List>

      <Container fixed>
        <div
          style={{
            display: "flex",
            flexDirection: "row",
            width: "19%",
            marginLeft: "44%",
            marginRight: "55%",
          }}
        >
          { applyJobList>=0? null: !checked ? <p>Visa</p> : null}

          


<div style={{display:"block", margin:"10px auto", width:"100%"}} >  


          <Checkbox
            color="default"
            required={checked}
            disabled={applyJobList <= 0}
            onChange={() => {
              setChecked(!checked);
            }}
            inputProps={{ "aria-label": "checkbox with default color" }}
          />
</div>

        </div>
        {checked && !loading   ? (
          <Button
          type="submit"
            color="primary"
            variant="contained"
            onClick={handleSubmit}
          >
            Skicka In
          </Button>
        ) : null}

        { submited ? (<CircularProgress color="secondary" />): null }
      </Container>
    </>
  );
};

export default ApplyList;

const useStyles = makeStyles((theme) =>
  createStyles({
    root: {
      display: "block",
      margin: "auto",
      width: "100%",
      maxWidth: 360,
      backgroundColor: theme.palette.background.paper,
      position: "relative",
      overflow: "auto",
      maxHeight: 300,
    },
    listSection: {
      backgroundColor: "inherit",
    },
    ul: {
      backgroundColor: "inherit",
      padding: 0,
    },
    ListItem: {
      display: "block",
      margin: "auto",
      width: "40%",
      textAlign: "center",
    },
  })
);
