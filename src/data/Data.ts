export interface row{
  id: string;
  company: String;
  contact: String;
  favorite: Boolean;
}


export const company1=  {
    id: "0",
    company: "",
    contact: "",
    profession: [""],
    favorite: false,
    
}as row;

export const company2=  {
  id: "1",
  company: "",
  profession: [],
  contact: "",
  favorite: false,
}as row;


export const company3=  {
  id: "2",
  company: "",
  contact: "",
  profession: [""],
  favorite: false,
}as row;


export const company4=  {
  id: "3",
  company: "ordic",
  contact: "work@utb.Nordic.se",
  profession: [""],
  favorite: false,
  }as row;

export const company5=  {
  id: "4",
  company: "Hbo",
  contact: "work@utb.hbo.se",
  profession: [""],
  favorite: false,
}as row;


export enum Teknologier {
  Java = "java",
  Typescript = "Typescript",
  Php = "Php",
  Linux = "Linux",
  Javascript = "Javascript",
  C = "C",
  Apputveckling = "Apputveckling",
  Testare = "Testare",
  Python = "Python",
  NET = "NET",
}

