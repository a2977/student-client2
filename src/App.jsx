import React, {useEffect, useMemo, useState} from "react";
import {BrowserRouter as Router, Switch, Route} from "react-router-dom";
import axios from "axios";

import "./App.css";
import Homepage from "./screens/Homepage";
import MyPages from "./screens/MyPages";
import LiaTable from "./screens/LiaTable";
import ApplyPage  from "./screens/ApplyPage";
import AppContext from "./components/AppContext";
import { company1, company2, company3, company4, company5 } from "./data/Data";
import CompletePage from "./screens/CompletePage";
import TestUpload from "./components/TestUpload";
import LoginPage from "./components/login";


import { ReactKeycloakProvider, } from '@react-keycloak/web'

import keycloak from './keycloak'



const qs = require('querystring');






function App() {
  const [job1, setJob1] = useState(company1);
   const [job2, setJob2] = useState(company2);
    const [job3, setJob3] = useState(company3);
    const [job4, setJob4] = useState(company4);
    const [job5, setJob5] = useState(company5);
    const [fullName, setFullName] = useState("");
    const [dbData, setDbData] = useState(null)
    const [education, setEducation] = useState("");
    const [resume, setResume] = useState("https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcT9Dp77rUv4mqKa994EeX3m4b-931qwG5HGkcSqYmEqS1K2GmGo82QS-VuAiU4Fno1DTp4&usqp=CAU")
    const [profile, setProfile] = useState("https://cdn.pixabay.com/photo/2015/10/05/22/37/blank-profile-picture-973460_960_720.png");
    const [linkedin, setLinkedin] = useState("");
    const [errorMessage, setErrorMessage] = useState("")
    const [tokenType, setTokenType] = useState(null)
    const [accessToken, setAccessToken] = useState(null)
    const [refreshToken, setrefreshToken] = useState(null)

    
    const [client , setclient] = useState("")

    const [student, setStudent] = useState(null);

    if (keycloak?.authenticated){
      setclient("login-app")
    }


    useEffect(() => {
      const body = {
        username: 'shu',
        password: 'shu',
        grant_type: "password",
        client_id: `${client}`
      }
      const config = {
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded',
        },
        mode: 'no-cors'

      }
      
axios.post('http://localhost:8080/auth/realms/SpringBootKeycloak/protocol/openid-connect/token',qs.stringify(body), config  
).then(res => {
  
  console.log(res.data)
  setAccessToken(res.data.access_token)
  setrefreshToken(res.data.refresh_token)
  setTokenType(res.data.token_type)

})
.catch(err => console.log(err))


    }, [client])


    
    useEffect( () => {
      const body = {
        username: 'shu',
        password: 'shu',
        grant_type: "password",
        client_id: `${client}`
      }
      const config = {
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded',
        },
        mode: 'no-cors'

      }
      
      axios.get("http://localhost:8081/students/getStudentById/1", config, body
     )
     .then(res => {console.log(res)
     console.log(accessToken)
     
     })
     .catch(err => console.log(err))

   }, [])

   useEffect(() => {
    fetch("http://localhost:8081/students/getStudentById/0")
      .then((res) => res.json())
      .then((student) => {
        setStudent(student);
        console.log(student);
      })

      .catch((err) => err.message);
  }, [profile]);




    const [favoriteList, setFavoriteList] = useState([]);
  const [allCompanies, setAllCompanies] = useState([job1, job2,job3, job4,job5])
  const providedValues = useMemo(() => ({job1, setJob1, job2, setJob2, job3, setJob3, job4, setJob4, job5, setJob5, favoriteList, setFavoriteList,fullName, setFullName,education, setEducation,resume, setResume,linkedin, setLinkedin,profile, setProfile,allCompanies, setAllCompanies, errorMessage, setErrorMessage, dbData, setDbData, student, setStudent, refreshToken, setrefreshToken, accessToken, setAccessToken, tokenType, setTokenType}), [job1, setJob1, job2, setJob2, job3, setJob3, job4, setJob4, job5, setJob5, favoriteList, setFavoriteList, fullName, setFullName,education, setEducation,resume, setResume,linkedin, setLinkedin, profile, setProfile,allCompanies, setAllCompanies, errorMessage, setErrorMessage, dbData, setDbData,student, setStudent, refreshToken, setrefreshToken, accessToken, setAccessToken, tokenType, setTokenType])
  const eventLogger = (event, error) => {
    console.log('onKeycloakEvent', event, error)
  }
  const tokenLogger = (tokens) => {
    console.log('onKeycloakTokens', tokens)
  }
  return (
    <ReactKeycloakProvider
    authClient={keycloak}
    onEvent={eventLogger}
    onTokens={tokenLogger}
  >
    <AppContext.Provider value={providedValues }>
      <div className="App">
        <Router>
          <Switch>
          <Route exact path="/" component={Homepage} />
            <Route exact path="/minasidor" component={MyPages} />
            <Route exact path="/table" component={LiaTable} /> 
            <Route exact path="/ansök" component={ApplyPage} /> 
            <Route exact path="/klart" component={CompletePage} /> 
            <Route exact path="/test" component={TestUpload} /> 
            <Route exact path="/login" component={LoginPage} /> 
          </Switch>
        </Router>
      </div>
      </AppContext.Provider >
      </ReactKeycloakProvider>
  );
}


export default App;


