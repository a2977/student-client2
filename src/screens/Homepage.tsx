import React from "react";
import Button from "@material-ui/core/Button";
import { useHistory } from "react-router";
import { AppBar, Container, Toolbar } from "@material-ui/core";
import Typography from "@material-ui/core/Typography";
import { makeStyles } from "@material-ui/core/styles";
import { useCallback } from 'react'
import { Redirect, useLocation } from 'react-router-dom'

import { useKeycloak } from '@react-keycloak/web'

const Homepage = () => {
  const classes = useStyles();

  const history = useHistory();
  const location = useLocation<{ [key: string]: unknown }>()
  const currentLocationState = location.state || {
    from: { pathname: '/' },
  }

  const { keycloak } = useKeycloak()

  const login = useCallback(() => {
    keycloak?.login()
  }, [keycloak])


  return (
    <>
      <AppBar position="relative" style={{ marginBottom: "200px" }}>
        <Toolbar>
          <Typography
            variant="h5"
            color="inherit"
            align="center"
            style={{ width: "100%" }}
          >
            Student
          </Typography>
        </Toolbar>
      </AppBar>
      <Container
        fixed
        maxWidth="sm"
        style={{ display: "flex", flexDirection: "column" }}
      >
        <h3 className={classes.text3}>
          Du har lyckats ta dig till Student-Clienten
        </h3>
        <Button
          className={classes.btns}
          variant="contained"
          color="primary"
          onClick={() => history.push("/table")}
        >
          Lia Platser
        </Button>
        <Button
        className={classes.btns}
          variant="contained"
          color="primary"
          onClick={() => history.push("/minasidor")}
        >
          Mina Sidor
        </Button>
        {
!keycloak?.authenticated ?  

<Button
          variant="contained"
          color="primary"
          onClick={login }
        >
          Logga in
        </Button>:
        null
        }

              
        
      </Container>
    </>
  );
};

const useStyles = makeStyles((theme) => ({
  btns: {
    marginBottom: "30px",
  },
  text3: {
    marginBottom: "100px",
  },
}));
export default Homepage;
