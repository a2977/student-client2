import React from "react";

import { AppBar, Button, List, Toolbar } from "@material-ui/core";
import KeyboardBackspaceIcon from "@material-ui/icons/KeyboardBackspace";

import Typography from "@material-ui/core/Typography";
import { useHistory } from "react-router";
import ListSubheader from "@material-ui/core/ListSubheader";
import CheckIcon from '@material-ui/icons/Check';


const CompletePage = () => {
  const history = useHistory();
  return (
    <div>
      <div>
        <AppBar position="relative" style={{ marginBottom: "120px" }}>
          <Toolbar>
            <KeyboardBackspaceIcon onClick={() => history.push("/")} />
            <Typography
              variant="h5"
              color="inherit"
              align="center"
              style={{ width: "100%" }}
            >
             Ansökan är Godkännd
        
            </Typography>
          </Toolbar>
        </AppBar>
      </div>

      <div style={{width: "100%", height:"200px"}} >
      <CheckIcon/>

      </div>

      <List
      sx={{
        width: '100%',
        maxWidth: 360,
        bgcolor: 'background.paper',
        position: 'relative',
        overflow: 'auto',
        maxHeight: 300,
        '& ul': { padding: 0 },
      }}
      subheader={<li />}
    >
        <li >
          <ul>
            <p>Vid dålig response uppdatera:</p>
            <ListSubheader>Linkedin</ListSubheader>
            <ListSubheader>Github</ListSubheader>
            <ListSubheader> Ev. bygg fler projekt</ListSubheader>
          </ul>
          <Button  variant="contained" color="primary"   onClick={() => history.push("/")}>
          Tipsa en vän
        </Button>
        </li>
    </List>
   
    </div>
  );
};

export default CompletePage;
