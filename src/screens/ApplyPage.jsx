import React, { useState } from "react";
import ApplyList from "../components/ApplyList";
import NavBar from "../components/NavBar";

const ApplyPage = () => {
  return (
    <div  >
      <NavBar />
      <ApplyList />
    </div>
  );
};

export default ApplyPage;
