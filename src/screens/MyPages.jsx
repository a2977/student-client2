import { Divider } from "@material-ui/core";
import Grid from "@material-ui/core/Grid";
import Paper from "@material-ui/core/Paper";
import { makeStyles } from "@material-ui/core/styles";
import TextField from "@material-ui/core/TextField";
import Typography from "@material-ui/core/Typography";
import React, { useContext, useState } from "react";
import "../App.css";
import Button from "@material-ui/core/Button";
import Toolbar from "@material-ui/core/Toolbar";
import HomeIcon from "@material-ui/icons/Home";
import AppBar from "@material-ui/core/AppBar";
import AppContext from "../components/AppContext";

import Upload from "../components/Upload";
import { useHistory } from "react-router";
import UploadResume from "../components/UploadResume";

const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
    flexDirection: "column",
    flexGrow: 1,
    overflow: "hidden",
    // padding: theme.spacing(0, 1),
    "& > *": {
      margin: theme.spacing(1),
    },
  },
  paper: {
    maxWidth: 1000,
    margin: `${theme.spacing(1)}px auto`,
    //padding: theme.spacing(2),
  },
  square: {
    width: 150,
    height: 150,
  },
  icon: {},
}));

export default function MyPages() {
  const history = useHistory();
  const {
    setProfile,
    linkedin,
    setLinkedin,
    fullName,
    setFullName,
    education,
    setEducation,
  } = useContext(AppContext);




  const classes = useStyles();


  return (
    <div>
      <AppBar position="relative">
        <Toolbar>
          <HomeIcon
            className={classes.icon}
            onClick={() => history.push("/")}
          />
          <Typography
            variant="h5"
            color="inherit"
            align="center"
            style={{ width: "100%" }}
          >
            Mina Sidor
          </Typography>
        </Toolbar>
      </AppBar>
      <div className={classes.root}>
        <Paper className={classes.paper} style={{ marginTop: 50 }}>
          <Grid container wrap="nowrap">
            <Grid item xs zeroMinWidth>
              <Upload />
            </Grid>
          </Grid>
        </Paper>

        <Grid className="profileTextSettings">
          <Grid container wrap="nowrap">
            <Grid item xs>
                <TextField
                  id="filled-textarea"
                  label="Fullständigt Namn"
                  placeholder={fullName}

                  multiline
                  value={fullName}
                  variant="filled"
                  onChange={(e) => {
                      setFullName(e.target.value);
                  }}
                />

            </Grid>
          </Grid>
          <Grid container wrap="nowrap" style={{ marginTop: "30px" }}>
            <Grid item xs>
                <TextField
                  id="filled-textarea"
                  label="Utbildning / Teknologier"
                  multiline
              placeholder={education}
                  value={education}
                  variant="filled"
                  onChange={(e) => {
                    setEducation(e.target.value);
                  }}
                />

            </Grid>
          </Grid>
        </Grid>
        <Divider style={{ margin: 40, backgroundColor: "#000000" }} />

        <div style={{ marginTop: 30 }}>
            <TextField
              id="filled-textarea"
              label="Länkaddres Linkedin"
              placeholder={linkedin}
              multiline
              value={linkedin}
              variant="filled"
              onChange={(e) => {
                setLinkedin(e.target.value);
              }}
            />

        </div>
        <Paper className={classes.paper} style={{ marginTop: 50 }}>
          <Grid container wrap="nowrap">
            <Grid item xs zeroMinWidth>
              <UploadResume />
            </Grid>
          </Grid>
        </Paper>

        <div>

          <Button
            variant="contained"
            color="primary"
            onClick={() => history.push("/table")}
          >
        Lediga Lia Platser
            
          </Button>
        </div>
      </div>
    </div>
  );
}
